<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')



  {{-- Фрейм с ошибками --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
  <div class="panel-body">

      <!-- Данные студента -->
        <label for="students" class="col-sm-3 control-label">Добавить студента в эту группу</label>

        <form action="{{ route('groups.students.index', $group) }}" method="POST">
        @csrf
  <div class="input-group">
      <div class="row">
    <div class="col">ФИО</div>
    <div class="col">Дата рождения</div>
    <div class="w-100"></div>

    <div class="col">
      <input type="text" class="form-control" name="fio" placeholder="Васев Илья Ильич">
    </div>

<div class="col">
        <div class="form-group">
 <input type="date" name="dateofbirth" max="3000-12-31"
        min="1000-01-01" class="form-control" >
</div>
</div>


            <div class="col">
            </div>
        </div>

      <!-- Кнопка добавления -->
        <div class="col-sm-offset-3 col-sm-6">
          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Добавить студента
          </button>
        </div>
  </div>
</form>


  <!-- Текущие студенты -->
    <div class="panel panel-default">

      <div class="panel-body">
        <table class="table table-striped task-table">

          <!-- Тело таблицы -->
{{-- Средние оценки --}}
          <tbody>
            <div class="row">
              @foreach ($groupaverages as $aver)
              <div class="col">
                <th>{{$aver->name}} {{$aver->rate}}</th>
              </div>
              @endforeach
          </div>
{{-- Студенты --}}
            <div class="container">
          <div class="row">
        <div class="col-md-12">
        <h4 align="center">Средние оценки группы</h4>
                <table id="mytable" class="table table-bordred table-striped">
                  <h4 align="center">Студенты</h4>
                   <thead>
                  {{-- <th>ID</th> Убрали ID по новым требованиям --}}
                   <th>Данные студента</th>
                     <th>Группа</th>
                     <th>Успеваемость</th>
                    @foreach ($subjects as $s)
                      <th>{{$s->name}}</th>
                    @endforeach
                        <th>Анкета</th>
                       <th>Удалить</th>
                        <th>Оценки</th>
                   </thead>
                    @foreach ($students as $student)
                        <tbody>
                        <tr style="background-color:{{$color}}">
                          {{-- <td>{{$student->id}}</td>  Убрали ID по новым требованиям--}}
                        <td>
<form action="{{ route('groups.students.update', [$group, $student]) }}" method="POST" >
    @csrf
    @method ('PUT')
    <div class="form-row ">
    <div class="col">
      <input type="text" class="form-control" name="fio" value = "{{$student->fio}}">
    </div>
    <div class="col">
      <div class="form-group">
      <input type="date" name="dateofbirth" max="3000-12-31"
            min="1000-01-01" class="form-control" value = {{$student->dateofbirth}}>
      </div>
    </div>
    <div class="col">
          <button type="submit"  class="btn btn-info">
      <i class="fa fa-plus"></i> Изменить
        </button>
        </div>
    </div>
</form>
                        </td>
                        <td>{{$student->group->name}}</td>
                        @foreach ($globalaverage as $ga) {{-- Приходится прочёсывать, ибо ID записей != ID студентов --}}
                            @if ($ga->student_id == $student->id)
                                <th>{{$ga->rate}}</th>
                                @break
                            @endif
                        @endforeach

                        @foreach ($averages[$student->id] as $s)
                          <div class="col">
                            <th>{{$s->rate}}</th>
                          </div>
                        @endforeach
                        <td><a href={{$url = route('groups.students.rates.index', [$group , $student])}}>Глянь анкету!</a>
                        <td>
                          <form action="{{ route('groups.students.destroy', [$group, $student]) }}" method="POST">
                            @csrf
                            @method('DELETE')

                            <button type="submit" id="{{$student}}" class="btn btn-danger">
                              <i class="fa fa-btn fa-trash"></i>Удалить
                            </button>
                          </form>
                        </td>
                    <td>
                <form action="{{ route('groups.students.rates.store', [$group, $student ]) }}" method="POST">
                            @csrf
                            <div class="form-row ">
                            <div class="col-sm-6 my-1">
                        <input type="number" min=1 max=5 step=1 class="input-group" name="rate" placeholder="Введите оценку...">
                        <select class="custom-select mb-3" name="subject_id">
                          <option selected>Выбери предмет...</option>
                          @foreach ($subjects as $subject)
                          <option class="input-group" value={{$subject->id}}>{{$subject->name}}</option>
                          @endforeach
                        </select>
                        </div>
                        <div class="col-sm-3 my-1">
                      <button type="submit"  class="btn btn-default">
                    <i class="fa fa-plus"></i> Добавить оценку
                      </button>
                      </div>
                      </div>
                </form>
                    </td>
            </tr>
                        </tbody>
                      @endforeach
                </table>
            </div>
          </tbody>
        </table>
      </div>
    </div>
@endsection