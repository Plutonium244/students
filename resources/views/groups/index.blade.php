<!-- resources/views/tasks/index.blade.php -->

@extends('layouts.app')

@section('content')

{{-- Фрейм с ошибками --}}
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

  <div class="panel-body">
      <!-- Данные -->
       <label for="groups" class="col-sm-3 control-label">Группа</label>
      <form action="{{ route('groups.index')}}" method="POST" class="form-horizontal">
      @csrf

  <div class="input-group">
      <div class="row">
    <div class="col">Группа</div>
    <div class="col">Описание</div>
    <div class="w-100"></div>

    <div class="col">
      <input type="text" class="form-control" name="name" placeholder="Название группы">
    </div>
    <div class="col">
      <input type="text" class="form-control" name="about" placeholder="Чем занимаются">
    </div>
        </div>
      <!-- Кнопка добавления -->
        <div class="col">
          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Добавить группу
          </button>
        </div>
  </div>
</form>


  <!-- Текущие студенты -->
    <div class="panel panel-default">

      <div class="panel-body">
        <table class="table table-striped task-table">

          <!-- Тело таблицы -->
          <tbody>
            <div class="container">
          <div class="row">
        <div class="col-md-12">
        <h4 align="center">Группы</h4>
        <div class="table-responsive">

                <table id="mytable" class="table table-bordred table-striped">
                   <thead>
                   <th>ID</th>
                   <th>Данные группы</th>
                   <th>Общая средняя оценка</th>
                    @foreach ($subjects as $subject)
                    <th>{{$subject->name}}</th>
                    @endforeach
                      <th>Посмотреть студентов группы</th>
                       <th>Удалить</th>
                   </thead>
                   <?php $iterator = 0;?> {{-- нужен! --}}
                   @foreach ($groups as $group)

                        <tbody>
                        <tr>
                        <td>{{$group->id}}</td>
<td>
<form action="{{ route('groups.update', [$group]) }}" method="POST" >
    @csrf
    @method ('PUT')
    <div class="form-row ">
    <div class="col">
      <input type="text" class="form-control" name="name" value = "{{$group->name}}">
    </div>
    <div class="col">
      <input type="text" class="form-control" name="about" value = "{{$group->about}}">
    </div>
    <div class="col">
          <button type="submit"  class="btn btn-info">
      <i class="fa fa-plus"></i> Изменить
        </button>
        </div>
    </div>
</form>
</td>
                    <th>{{$globalaverage[$iterator]->rate}}</th>
                    <?php $iterator++;?> {{-- Итератор нужен для того, чтобы из массива вытаскивать общие средние в совпадении, это дешевле, чем сравнивать Group id постоянно. И удобнее.  --}}
                    @foreach ($averages[$group->id] as $a)
                      <th>{{$a->rate}}</th>
                    @endforeach

                        <td><a href={{$url = route('groups.students.index', $group)}}>Посмотреть</a>
                        @csrf
                        </td>
                        <td>
                          <form action="{{ route('groups.destroy', $group)}}" method="POST" class="form-horizontal">
                            @csrf
                            @method('DELETE')

                            <button type="submit" id="{{$group->id}}" class="btn btn-danger">
                              <i class="fa fa-btn fa-trash"></i>Удалить
                            </button>
                          </form>
                        </td>

                        </tbody>
                      @endforeach
                </table>
              </div>
            </div>
          </tbody>
        </table>
      </div>
    </div>
   {{-- @endif --}}
@endsection