@extends('layouts.app')

@section('content')
<div class="container">
    {{--  Шапка для фильтрации --}}
    <h4 align="center">Фильтры</h4>
<form action="{{ route('home') }}" method="GET">
        @csrf
  <div class="input-group">
    <div class="row">
        <div class="col-lg-">
      <input type="text" name="fiofilter" placeholder="ФИО" value="{{$filterstring[0]}}">
        </div>
    <div class="col-lg-">
      <input type="text" name="groupfilter" placeholder="Группа" value="{{$filterstring[1]}}">
    </div>
    @foreach ($subjects as $subject)
        <div class="col-lg-">
    <input type="number" value={{$filterstring[$subject->id+1]}} min=1 max=5 step=0.1 name="{{$subject->id}}" placeholder="{{$subject->name}} ">
        </div>
    @endforeach
      <!-- Кнопка добавления -->
        <div class="col-sm-offset-3 col-sm-6">
          <button type="submit" class="btn btn-default">
            <i class="fa fa-plus"></i> Применить фильтры
          </button>
        </div>
    </div>
  </div>
</form>
    <table id="mytable" class="table table-bordred table-striped">
                  <h4 align="center">Студенты</h4>
                <thead>
                   <th>ФИО</th>
                   <th>Дата рождения</th>
                     <th>Группа</th>
                     <th>Успеваемость</th>
                    @foreach ($subjects as $s)
                      <th>{{$s->name}}</th>
                    @endforeach
                        <th>Анкета</th>
                       <th>Удалить</th>
                   </thead>
                   <tbody>
                    <tr>
                @foreach ($students as $student) {{-- В student[0] лежит студент, в student[1] - его общая средняя оценка, в student[2] - массив средних оценок по предметам. --}}
                        <td>{{$student->fio}}</td>
                        <td>{{$student->dateofbirth}}</td>
                        <td>{{$student->group->name}}</td>
                        @foreach ($averagerate as $av)
                                @if ($av->student_id == $student->id)
                                    <td>{{$av->rate}}</td>
                                @endif
                        @endforeach
                        @foreach ($averagerates as $av)
                            @foreach ($av as $a)
                                @if ($a->student_id == $student->id)
                                    <td>{{$a->rate}}</td>
                                @endif
                            @endforeach
                        @endforeach
                        <td><a href={{$url = route('groups.students.rates.index', [$student->group , $student])}}>Глянь анкету!</a>
                        <td>
                        <form action="{{ route('groups.students.destroy', [$student->group, $student]) }}" method="POST">
                            @csrf
                            @method('DELETE')

                        <button type="submit" id="{{$student}}" class="btn btn-danger">
                          <i class="fa fa-btn fa-trash"></i>Удалить
                        </button>
                        </th>
                        </form>
            </tr>
        </tbody>
        @endforeach
        {!! $students->appends(Request::except('page'))->render() !!}
    </table>
</div>
@endsection
