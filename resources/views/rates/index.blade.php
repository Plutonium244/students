
@extends('layouts.app')

@section('content')

{{-- Фрейм с ошибками --}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

  <!-- Bootstrap шаблон... -->

<div class="panel-body">

    <div class="panel panel-default">

      <div class="panel-body">
        <table class="table table-striped task-table">
          <tbody>
            <div class="row">
            <div class="col">
              @if (empty($student->image))
              <img src="{{ asset('upload/'."cat.jpg") }}">
              @else
              <img src="{{ asset('upload/'.$student->image) }}">
              @endif
              <form action="{{ route('groups.students.update', [$group, $student]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method ('PUT')
                <div class="form-group">
                  <label for="exampleFormControlFile1">Выберите новую фотографию</label>
                  <input type="file" class="file-select"  name="imagef" id = "imagef">
                </div>

            </div>
          <div class="col">
            Изменить данные студента
                  <div class="form-group row">
                    <input type="text" class="form-control" name="fio" value = "{{$student->fio}}">
                  </div>
                    <div class="form-group row">
                    <input type="date" name="dateofbirth" max="3000-12-31"
                          min="1000-01-01" class="form-control" value = {{$student->dateofbirth}}>
                  <div class="form-group row">
                    <select class="custom-select mb-3" name="group_id">
                      @foreach ($groups as $g)
                      @if ($g->id == $student->group_id )
                        <option selected value={{$g->id}}>{{$g->name}}</option>
                        {{-- <option class="input-group" value={{$g->id}}>{{$g->name}}</option> --}}
                      @else
                        <option class="input-group" value={{$g->id}}>{{$g->name}}</option>
                      @endif
                      @endforeach
                    </select>
                      <button type="submit"  class="btn btn-info">
                    <i class="fa fa-plus"></i> Сохранить изменения
                      </button>
                  </div>
                </form>
              </div>
            </div>
          </tbody>
        </table>
      </div>
    </div>
@endsection