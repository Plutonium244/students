<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'about' => 'required|string',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Введите название',
            'about.required'  => 'Ввведите описание',
            'about.string' => 'В описании разрешено использовать только буквы и цифры',
        ];
    }
}
