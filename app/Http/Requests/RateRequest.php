<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Subject;

class RateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        //$subjectidcollection = subject::all('id');
        return [
        'rate' => 'required|in:1,2,3,4,5', //проще перечислить оценки, чем задавать рамки и шаги
        'subject_id' => 'required|exists:Subjects,id',// $this->subjectidcollection,
        ];
    }
    public function messages()
    {
        //$subjectidcollection = subject::all('id as subject_id')->toArray();
        return [
            'rate.required' => 'Неверно введена оценка',
            'rate.in:1,2,3,4,5' => 'Неверно введена оценка',
            'subject_id.required'  => 'Неверно выбран предмет',
            'subject_id.exists:Subjects,id' => 'Неверно выбран предмет',
        ];
    }
}
