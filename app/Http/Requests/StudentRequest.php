<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\Factory as ValidationFactory;

class StudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(ValidationFactory $validationFactory)
    {

        $validationFactory->extend(
            'alphaspace',
            function ($attribute, $value, $parameters) {
                dump($value);
                $forpreg = '/[a-zа-яё -]*/i';
                dump(boolval(preg_match($forpreg, $value)));
                return boolval(preg_match($forpreg, $value));
            },
            'В строке должны быть алфавитные символы, пробелы и "-".'
        );
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        'fio' => 'required|alphaspace',
        'dateofbirth' => 'required|date',
        'imagef' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ];
    }
    public function messages()
    {
        return [
        'fio.required' => 'Введите ФИО',
        'dateofbirth.required'  => 'Введите дату',
        'dateofbirth.date'  => 'Неверный формат даты',
        'imagef.image' => 'Неверный формат изображения',
        'imagef.mimes:jpeg,png,jpg,gif,svg' => 'Неверный формат изображения',
        ];
    }
}
