<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StudentRepository;
use App\Http\Requests\StudentRequest;
use App\Models\Student;
use App\Models\Group;
use Carbon\Carbon;

class StudentController extends Controller
{
    protected $students;
    protected $subjects;
    public function __construct(StudentRepository $students)
    {
        $this->students = $students;
        //$this->subjects = $subjects;
    }

    public function index(Group $group)
    {
        dump ($group);
        return view('students.index', [
        'students' => $this->students->takeStudent($group->id),
        'subjects' =>$this->students->takeSubject(),
        'group' => $group,
        'globalaverage' => $this->students->takeAverageRate(),    // для цвта строки студента
        'averages'=> $this->students->takeAverageRates($group->id),
        'color' => "lightgrey",
        'groupaverages' => $this->students->takeAverageByGroup($group->id), // для группы.
        ]);
    }

    public function create()
    {
        return back();
    }

    public function store(Group $group, StudentRequest $request)
    {
        Student::create($request->all()+['group_id' => $group->id]);
        return back();
    }

    public function show($id)
    {
        return back();
    }

    public function edit($id)
    {
        //
    }

    public function update(Group $group, StudentRequest $request, $id)
    {
        if ($request->imagef != null) {
            $file = $request->imagef;
            $file->move(public_path() . '/upload', "$id.jpg");
            Student::find($id)->update($request->all()+['group_id' => $group->id]+['image' => "$id.jpg"]);
            return back();
        } else {
            Student::find($id)->update($request->all()+['group_id' => $group->id]);
            return back();
        }
    }
    public function destroy(Request $request, Group $group, Student $student)
    {
        $student->delete();
        return back();
    }
}
