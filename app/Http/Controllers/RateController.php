<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\RateRepository;
//use App\Http\Requests\RateRequest;
//use App\Models\Rate;
use App\Models\Student;
use App\Models\Group;

class RateController extends Controller
{
    protected $groups;

    public function __construct(RateRepository $groups)
    {
        $this->groups = $groups;
    }

    public function index(Group $group, Student $student)
    {
            return view('rates.index', [
            'group' => $group,
            'student' => $student,
            'groups' => $this->groups->takeGroup(),
            ]);
    }

    public function create()
    {
        //
    }

    public function store(Group $group, Student $student, RateRequest $request)
    {
        dump($request->id);
        Rate::create(
            $request->all()+
            ['student_id' => $student->id]
        );
        return back();
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function messages()
    {
        return [
            'rate.required' => 'введите верную оценку',
            'subject_id.required'  => 'Неправильно выбран предмет',
        ];
    }
}
