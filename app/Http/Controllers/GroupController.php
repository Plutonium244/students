<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\GroupRepository;
use App\Models\Group;
use App\Http\Requests\GroupRequest;

use App\Models\Rate;
use App\Models\Student; // нужны для ввода фейковых данных


class GroupController extends Controller
{
    protected $groups;

    public function __construct(GroupRepository $groups)
  {
    $this->groups = $groups;
  }
    public function index()
    {
        return view('groups.index', [
        'groups' => $this->groups->takeGroup(),
        'globalaverage' => $this->groups->takeGroupGlobalAverages(),
        'averages' => $this->groups->takeAveragesByGroups(),
        'subjects' => $this->groups->takeSubjects(),
        ]);
    }
    public function create()
    {
        return redirect('/groups');
    }

    public function store(Request $request)
    {
        Group::create($request->all());
        return redirect('/groups');
    }


    public function show($id)
    {
        return redirect('/groups');
    }

    public function edit($id)
    {
        //
    }

    public function update(GroupRequest $request, $id)
    {
        Group::find($id)->update($request->all());
        return back();
    }

    public function destroy(Request $request, Group $group)
    {
        $group->delete();
        return redirect('/groups');
    }
}
