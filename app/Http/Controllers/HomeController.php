<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\HomeRepository;
use App\Models\Group;
use Illuminate\Support\Facades\Route;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $filterstring;
    protected $students;
    protected $paginator = [];

    public function __construct(HomeRepository $students)
    {
        $this->middleware('auth');
        $this->filterstring = [null,null,null,null, null];
        $this->students = $students;
    }

    public function index(Request $request)
    {
        $this->filterstring[0] = $request->fiofilter;
        $this->filterstring[1] = $request->groupfilter;
        foreach ($this->students->takeSubject() as $subject) {
            $index = $subject->id;
            $this->filterstring[$index+1] = $request->$index;
        }
        return view('home', [
        'students' => $this->students->takeStudents($this->filterstring, $this->paginator),
        'subjects' => $this->students->takeSubject(),
        'averagerate' => $this->students->takeAverageRate(),
        'averagerates' => $this->students->takeAverageRates(),
        'filterstring' => $this->filterstring,
            ]);
    }
}
