<?php

namespace App\Repositories;

use App\Models\Student;
use App\Models\Group;
use App\Models\Subject;
use App\Models\Rate;
use Illuminate\Support\Facades\DB;

class HomeRepository
{
    public function takeStudents($filter, $pathstring)
    // принимает массив ключ-значение и строку с запросом-фильтром
    {
        $tempstudids = ($filter[0]!=null ?
        Student::select('id')->where('fio', 'like', '%'."$filter[0]".'%')->get():
        DB::table('students')->select('id')->get() );
        $temp = Student::where(function ($query) use ($filter) {
            if ($filter[0]!=null) {
                $query->where('fio', 'like', '%'."$filter[0]".'%');
            }
        })
        ->whereHas('group', function ($query) use ($filter) {
            if ($filter[1]!=null) {
                $query->where('name', 'like', '%'."$filter[1]".'%');
            }
        })
        ->whereHas('rates', function ($query) use ($filter, $tempstudids) {
            for ($i=2; $i<count($filter); $i++) {
                $averages = [];
            // В нулевом и первом значениях фильтра хранятся фамилия и группа. Оценки со второго.
                if ($filter[$i]!=null) {
                    foreach ($tempstudids as $id) {
                        foreach ($this->takeSubject() as $subject) {
                            $averages[$id->id][$subject->id] = DB::table('rates')
                            ->where('student_id', '=', "$id->id")
                            ->where('subject_id', '=', "$subject->id")
                            ->groupBy('student_id') //??
                            ->avg('rate');
                        }
                        if ($averages[$id->id][$i-1] >= $filter[$i])
                    $query->where('rate', '>', '0'); //эквивалентно "пропустить!"
                    else $query->where('student_id', '<>', "$id->id"); //эквивалентно "остановить!"
                         // -1 оттого, что ИД предметов идут с 1, а ИД фильтра на оценки - с двух
                    }
                }
            }
        });
        return    $temp->paginate(5);

        // Старый код, рабочий (но долгий). Получить коллекцию, фильтрануть, вывести с пагинацией.
        // $temp = Student::with('group')->get();
        // $temp = $temp->zip($this->takeAverageRate(), $this->takeAverageRates());
        // if ($filter[0]!=null)
        // $temp = $temp->filter(function ($value, $key) use ($filter) {
        // return boolval(strpos($value, $filter[0]));
        // });
        // if ($filter[1]!=null)
        // $temp = $temp->filter(function ($value, $key) use ($filter) {
        // return boolval(strpos($value, $filter[1]));
        // });
        // for ($i=2; $i<count($filter); $i++) {   // фильтр по всем предметам
        // if ($filter[$i]!=null)
        // $temp = $temp->filter(function ($value, $key) use ($filter, $i) {
        // return $value[2][$i-2]->rate >= $filter[$i];
        //     });
        // }
        // return    $this->paginateCollection($temp, 15);
    }
    public function takeAverageRates() // массив средних по конкретным предметам
    {
        $studs = Student::all();
        $temp = [];
        foreach ($studs as $s) {
            $temp[$s->id] = DB::table('rates')
            ->select('subject_id', 'student_id', DB::raw('AVG(rate)as "rate"'))
            ->where('student_id', $s->id)
            ->groupBy('subject_id')
            ->get();
        }
        return    $temp;
    }
    public function takeAverageRate() // массив средних студента по всем предметам
    {
        $temp = DB::table('rates')
        ->select('subject_id', 'student_id', DB::raw('AVG(rate)as "rate"'))
          ->groupBy('student_id')
          ->get();
        return    $temp;
    }
    public function takeSubject()
    {
            return    subject::all();
    }
    private function paginateCollection($collection, $perPage, $pageName = 'page', $fragment = null)
    {
        $currentPage = \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPage($pageName);
        $currentPageItems = $collection->slice(($currentPage - 1) * $perPage, $perPage);
        parse_str(request()->getQueryString(), $query);
        unset($query[$pageName]);
        $paginator = new \Illuminate\Pagination\LengthAwarePaginator(
            $currentPageItems,
            $collection->count(),
            $perPage,
            $currentPage,
            [
                'pageName' => $pageName,
                'path' => \Illuminate\Pagination\LengthAwarePaginator::resolveCurrentPath(),
                'query' => $query,
                'fragment' => $fragment
            ]
        );

        return $paginator;
    }
}
