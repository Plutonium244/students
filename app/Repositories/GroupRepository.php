<?php

namespace App\Repositories;

use App\Models\Group;
use App\Models\Subject;
use Illuminate\Support\Facades\DB;


class GroupRepository
{
  public function takeGroup()
  {
        return    group::all();
  }
  public function takeGroupGlobalAverages()
  {
  	// взять общую среднюю
  		$temp = DB::table('rates')
  		->join('students', 'students.id', '=', 'student_id')
  		->join('groups', 'groups.id', '=', 'students.group_id')
  		->select('group_id', DB::raw('AVG(rate)as "rate"'))
  		->groupBy('group_id')
  		->get();
        return    $temp;
  }
  public function takeAveragesByGroups()
  {
  	$groups = $this->takeGroup();
  	$temp = [];
  	foreach ($groups as $g) { // взять средние по каждому предмету
  		$temp[$g->id] = DB::table('rates')
  		->join('students', 'students.id', '=', 'student_id')
  		->join('groups', 'groups.id', '=', 'students.group_id')
  		->join('subjects', 'subjects.id', '=', 'rates.subject_id')
  		->where('group_id', $g->id)
  		->select('subjects.name', 'group_id', DB::raw('AVG(rate)as "rate"'))
  		->groupBy('subjects.id')
  		->get();
  	}
        return    $temp;
  }
  public function takeSubjects()
  {
        return    subject::all('name');
  }

}