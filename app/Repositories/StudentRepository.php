<?php

namespace App\Repositories;

use App\Models\Student;
use App\Models\Subject;
use App\Models\Rate;
use Illuminate\Support\Facades\DB;


class StudentRepository
{
  public function takeStudent($id)
  {
        return    Student::where('group_id', $id)->get();
  }
  public function takeSubject()
  {
        return    subject::all();
  }
  public function takeAverageRates($id) // массив средних по конкретным предметам
  {
      $studs = $this->takeStudent($id);
      $temp = [];
      foreach ($studs as $s) {
  		$temp[$s->id] = DB::table('rates')
  		->select('subject_id','student_id', DB::raw('AVG(rate)as "rate"'))
      ->where('student_id', $s->id)
  		->groupBy('subject_id')
  		->get();
      }
        return    $temp;
  }
  public function takeAverageRate() // массив средних студента по всем предметам
  {
      $temp = DB::table('rates')
      ->select('subject_id', 'student_id', DB::raw('AVG(rate)as "rate"'))
      ->groupBy('student_id')
      ->get();
      //dump($temp);
        return    $temp;
  }
  public function takeAverageByGroup($id)
  {
  		$temp = DB::table('rates')
  		->join('students', 'students.id', '=', 'student_id')
  		->join('groups', 'groups.id', '=', 'students.group_id')
  		->join('subjects', 'subjects.id', '=', 'rates.subject_id')
  		->where('group_id', $id)
  		->select('subjects.name', 'students.group_id', DB::raw('AVG(rate)as "rate"'))
  		->groupBy('subjects.name')
  		->get();
        return    $temp;
  }
}