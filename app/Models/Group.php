<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Group extends Eloquent
{
    protected $fillable = [
        'name',
        'about'
    ];

    public function students()
    {
        return $this->hasMany(\App\Models\Student::class);
    }
}
