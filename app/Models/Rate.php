<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Rate extends Eloquent
{
    protected $casts = [
        'subject_id' => 'int',
        'student_id' => 'int',
        'rate' => 'int'
    ];

    protected $fillable = [
        'subject_id',
        'student_id',
        'rate'
    ];

    public function student()
    {
        return $this->belongsTo(\App\Models\Student::class);
    }

    public function subject()
    {
        return $this->belongsTo(\App\Models\Subject::class);
    }
}
