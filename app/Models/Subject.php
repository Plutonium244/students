<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

class Subject extends Eloquent
{
    protected $fillable = [
        'name'
    ];

    public function rates()
    {
        return $this->hasMany(\App\Models\Rate::class);
    }
}
