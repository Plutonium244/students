<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 14:06:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Student
 * 
 * @property int $id
 * @property string $fio
 * @property \Carbon\Carbon $dateofbirth
 * @property int $group_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $image
 * 
 * @property \App\Models\Group $group
 * @property \Illuminate\Database\Eloquent\Collection $rates
 *
 * @package App\Models
 */
class Student extends Eloquent
{
	protected $casts = [
		'group_id' => 'int'
	];

	protected $dates = [
		'dateofbirth'
	];

	protected $fillable = [
		'fio',
		'dateofbirth',
		'group_id',
		'image'
	];

	public function group()
	{
		return $this->belongsTo(\App\Models\Group::class);
	}

	public function rates()
	{
		return $this->hasMany(\App\Models\Rate::class);
	}
}
