<?php

use Illuminate\Database\Seeder;
use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Rate;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    	{
			    	factory(App\Models\Group::class, 2)->create(); // Работает, пока не генерим

			    	foreach (group::all() as $group) {
			    		factory(App\Models\Student::class, 3)->create(['group_id'=>$group->id]);
			    	}

					foreach (student::all() as $student) {
						foreach (subject::all() as $subject) {
							factory(App\Models\Rate::class, 6)
							->create([
								'student_id'=>$student->id,
								'subject_id'=>$subject->id,
							]);
						}
					}
		}
}