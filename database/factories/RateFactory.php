<?php

use Faker\Generator as Faker;
use Faker\Generator\Factory;
use App\Models\Rate;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

    $factory->define(Rate::class, function (Faker $faker) {
    return [
        'rate' => $faker->randomElement($array = array ('1','2','3','4','5')),
    ];});
