<?php

use Faker\Generator as Faker;
use Faker\Generator\Factory;
use App\Models\Student;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

    $factory->define(Student::class, function (Faker $faker, $group_id) {
    	
    return [
        'fio' => $faker->name,
        'dateofbirth' => $faker->date($format = 'Y-m-d', $max = 'now', $min = '30-01-1950'),
        'group_id' => $group_id,
    ];
});