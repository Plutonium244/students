<?php

use Faker\Generator as Faker;
use Faker\Generator\Factory;
use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Rate;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

	$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];});

    // $factory->define(Student::class, function (Faker $faker) {
    //     dump((group::all()->pluck('id')));
    // return [
    //     'fio' => $faker->name,
    //     'dateofbirth' => $faker->date($format = 'Y-m-d', $max = 'now', $min = '30-01-1950'),
    //     'group_id' => array_rand(array_column(group::all('id')->toArray(), "id")),
    // ];});
    // $factory->define(Group::class, function (Faker $faker) {
    // return [
    //     'name' => $faker->sentence(1),
    //     'about' => $faker->sentence(4),
    // ];});
    // $factory->define(Rate::class, function (Faker $faker) {
    // return [
    //     'rate' => $faker->random_int(1, 5),
    //     'subject_id' => array_rand(subject::all('id')->toArray()),
    //     'student_id' => array_rand(student::all('id')->toArray()),
    // ];});
